﻿#include <iostream>
//#include "Helpers.h"
#include <string>
#include <math.h>

using namespace std;

template <class T>
class Stack {
public:
	Stack(int size=10):stackSize(size)
	{}
	T pop() 
	{
		counter--;
		return *(ptr + counter);
		
	}
	void push(const T x) 
	{
		*(ptr+ counter) = x;
		counter++;
		
	}
	~Stack() {
		delete[] ptr;
	}
private:
	int stackSize;
	T* ptr = new T[stackSize];
	int counter = 0;
};

class Animal {
public:
	virtual void Voice() {
		cout << "Animal say\n";
	}
};

class Dog :public Animal {
public:
	Dog(){}
	void Voice() override {
		cout << "Dog say Woof!\n";
	}
};

class Cat :public Animal {
public:
	Cat() {}
	void Voice() override {
		cout << "Cat say Maay\n";
	}
};

class Elephant :public Animal {
public:
	Elephant() {}
	void Voice() override {
		cout << "Elephant say Yyyyy\n";
	}
};

class Hourse :public Animal {
public:
	Hourse() {}
	void Voice() override {
		cout << "Hourse say iii-go-go\n";
	}
};

int main()
{
	setlocale(0, "");
	//Animal* may = new Cat;
	//Animal* wof = new Dog;
	Cat Tom;
	Dog Spike;
	Elephant Dambo;
	Hourse Agat;
	Animal* all[10];// = new Animal[10];
	for (int i = 0; i < 10;i++) {
		if (i % 2 == 0) all[i] = &Tom;
		else if(i % 3 == 0) all[i] = &Spike;
		else if (i % 4 == 0) all[i] = &Dambo;
		else all[i] = &Agat;
	}
	for (int i = 0; i < 10; i++) {
		all[i]->Voice();
	}
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
